# Protractor Demo for Backbase

## TODO
* [x]setting up some tests
* [x]doing some image comparison
* [x]showing some Page Objects Model - magic
* [x]connecting with SauceLabs
* [x]connecting with BrowserStack


## Requirements
- npm 12.1.0 (node.js package manager)
- latest chrome

## Installing
run `npm install` to install all dependencies

## Running Locally
run the tests locally with `npm run e2e`.

### Running on SauceLabs
to run the tests on SauceLabs run the following commands in your terminal:
```
export SAUCE_USERNAME='<your_saucelabs_username>
export SAUCE_ACCESS_KEY='<your_saucelabs_access_key>'
```

`npm run sauce`

### Running on BrowserStack
to run the tests on BrowserStack run the following commands in your terminal:

```
export BROWSERSTACK_USERNAME='<your_browserstack_username>'
export BROWSERSTACK_ACCESS_KEY='<your_browserstack_accesskey>'
```

`npm run sauce`

## Issues
If you run the test against saucelabs, it can happen that you will run into a timeout.
The tests will run fine, the results are ok but saucelabs will still mark the test as 'failed' because of this protractor issue:
https://github.com/angular/protractor/issues/4817



## Tools I used
* Visual Studio Code
* iTerm2 (Oh my ZSH)
* Firefox Developer Edition
