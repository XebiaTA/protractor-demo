const { join } = require('path');

exports.config = {
    plugins: [
        {
            package: 'protractor-image-comparison',
            options: {
                baselineFolder: join(process.cwd(), './baseline/'),
                formatImageName: `{tag}-{logName}-{width}x{height}`,
                screenshotPath: join(process.cwd(), '.tmp/'),
                savePerInstance: true,
                autoSaveBaseline: true
            },
        },
    ],
    framework: 'jasmine',
    baseUrl: 'https://www.google.com/',
    sauceUser: process.env.SAUCE_USERNAME,
    sauceKey: process.env.SAUCE_ACCESS_KEY,
    sauceSeleniumUseHttp: true,
    sauceSeleniumAddress: 'ondemand.eu-central-1.saucelabs.com:80/wd/hub',
    allScriptsTimeout : 200000,
    restartBrowserBetweenTests: true,
    // restartBrowserBetweenTests: true,
    // idleTimeout: 1800,
   
    suites: {
        search: 'e2e/search/**/*_spec.js'
      },

    multiCapabilities: [{
        extendedDebugging: true,
        browserName: 'firefox',
        version: 'latest',
        platform: 'OS X 10.14',
        name: 'firefox-tests',
        shardTestFiles: false,
        maxInstances: 25,
        username: process.env.SAUCE_USERNAME,
        accessKey: process.env.SAUCE_ACCESS_KEY,
        build: 1
    }, {
        extendedDebugging: true,
        browserName: 'chrome',
        version: '75',
        platform: 'Windows 7',
        name: 'chrome-tests',
        shardTestFiles: true,
        maxInstances: 25,
        username: process.env.SAUCE_USERNAME,
        accessKey: process.env.SAUCE_ACCESS_KEY
    }],

    onComplete: function (success) {
        browser.quit();
        var printSessionId = function (jobName) {
            browser.getSession().then(function (session) {
                console.log('SauceOnDemandSessionID=' + session.getId() + ' job-name=' + jobName);
            });
        }
        printSessionId("some job");
        if (!success) {
            process.exit(1);
        }
    },
    onCleanUp: function(exitCode) {
       if (exitCode ==1){
           console.log("Getting out");
           browser.quit();
       };
       if (exitCode ==0){
        console.log("Getting out");
            browser.quit()
       }
    },
};