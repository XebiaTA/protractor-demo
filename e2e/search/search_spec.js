import {$, browser} from 'protractor';

var search = require('./search_pom');

describe('Demo: Google Search', async() => {
  beforeEach( () => {
    browser.waitForAngularEnabled(false);
    browser.get(browser.baseUrl);
  });

  it('should have a title', () => {
    expect(browser.getTitle()).toEqual('Google');
  });

  it('should search for Xebia Digital Assurance', async () => {
    await search.getSearchInputField().sendKeys('Xebia Digital Assurance');
    
    // Image Comparing
    await browser.imageComparison.saveScreen('keystroke', { /* some options*/ });
    expect(await browser.imageComparison.checkScreen('keystroke', { /* some options*/ })).toEqual(0,5);

    // Submit Search 
    await search.getSearchForm().submit();
    // throwing in an anti-pattern.
    browser.sleep(10000);

    //Search Results Comparing
    // issue with not showing list results in checkScreen (they are there, cauz the next click action will work)
    await browser.imageComparison.saveScreen('searchResults', { /* some options*/ });
    expect(await browser.imageComparison.checkScreen('searchResults', { /* some options*/ })).toEqual(0,5);


    await search.getXebiaLinkFromSearchResults().click();

    expect(await browser.getCurrentUrl()).toEqual('https://pages.xebia.com/digital-assurance');

  });

});