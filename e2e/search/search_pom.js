var Search = function() {
    var inputField = element(by.css('.gLFyf.gsfi'));
    var searchForm = element(by.id('tsf'));
    var XebiaLinkFromSearchResults = element.all(by.css('a[href*="https://pages.xebia.com/digital-assurance"]')).first();

    this.getSearchInputField = function() {
        return inputField;
    }

    this.getSearchForm = function() {
        return searchForm;
    }

    this.getXebiaLinkFromSearchResults = function() {
        return XebiaLinkFromSearchResults;
    }
  };
  module.exports = new Search();
